import Vue from "vue";
import "./plugins/vuetify";
import App from "./App.vue";

import router from "./router/";
import store from "./store";
import "./registerServiceWorker";
import "roboto-fontface/css/roboto/roboto-fontface.css";
import "font-awesome/css/font-awesome.css";
import "vue-material-design-icons/styles.css";
import service from "@/services/Service";
Vue.config.productionTip = false;
Vue.prototype.$service=service;

Vue.prototype.$bus = new Vue();
new Vue({
  router,
  store,
  render: h => h(App)
}).$mount("#app");
