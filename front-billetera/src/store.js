import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

const LOGIN = "LOGIN";
const LOGIN_SUCCESS = "LOGIN_SUCCESS";
const LOGOUT = "LOGOUT";

export default new Vuex.Store({
  state: {
    notify: {
      show: false,
      estado: 0,
      _xtra: null
    },
    token: '',
    menu: []

  },
  mutations: {
    alerta(state, payload) {
      state.notify.show = payload.show;
      state.notify.estado = payload.estado;
      (payload._xtra) ?
        state.notify._xtra = payload._xtra :
        state.notify._xtra = null;
    },
    SET_TOKEN(state, payload) {
      state.token = payload;
    },
    SET_MENU(state, payload) {
      console.log(payload);
      state.menu = payload;
    }

  },
  actions: {}
});
