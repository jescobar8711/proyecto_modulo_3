import axios from 'axios';
const apiClient = axios.create({
  baseURL:'http://localhost:85/api/',
  withCredentials:false,
  headers:{
    Accept:'application/json',
  //'Content-Type':'application/json',
 'Content-Type':'application/x-www-form-urlencoded',
  // 'X-Powered-By':'Express',

  }
});


export default {
  get(url,data =''){
    return apiClient.get(url,data)
  },
  post(url,data){
    return apiClient.post(url,data)
  }
}