import Vue from "vue";
import Router from "vue-router";
import paths from "./paths";
import NProgress from "nprogress";
import "nprogress/nprogress.css";


Vue.use(Router);

const router = new Router({
  base: "/",
  mode: "hash",
  linkActiveClass: "active",
  routes: paths
});

router.beforeEach((to, from, next) => {
  //window.getApp.$emit('APP_LOADING',true,'CARGANDO DATOS');
  NProgress.start();
  next();
});

router.afterEach((to, from) => {
  NProgress.done();
  //window.getApp.$emit('APP_LOADING',false);
});

export default router;
