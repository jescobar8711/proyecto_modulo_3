export default [
  
  { // si ingresa cualquier direccion que no sea la correcta
    path: '*',
    meta: {
      public: true,
    },
    redirect: {
      path: '/404'
    }
  },
  {
    path: '/404',
    meta: {
      public: true,
    },
    name: 'NotFound',
    component: () => import(
      `@/views/NotFound.vue`
    )
  },
  {
    path: '/403',
    meta: {
      public: true,
    },
    name: 'AccessDenied',
    component: () => import(
      `@/views/Deny.vue`
    )
  },
  {
    path: '/500',
    meta: {
      public: true,
    },
    name: 'ServerError',
    component: () => import(
      `@/views/Error.vue`
    )
  },
  {
    path: '/',
    meta: { breadcrumb: true, autenticado: true },
    name: 'Dashboard',
    component: () => import(
      `@/views/Dashboard.vue`
    )
  },
  
  /*INICIO UNIDAD DE PLANIFICACION  */
  {
    path: "/billetera/usaurio",
    meta: { breadcrumb: true },
    name: "usuario",
    component: () =>
      import(/* webpackChunkName: "routes" */
      `@/views/ui/Usuario.vue`)
  },
  {
    path: "/billetera/retiro",
    meta: { breadcrumb: true },
    name: "retiro",
    component: () =>
      import(/* webpackChunkName: "routes" */
      `@/views/ui/Retiro.vue`)
  },
  {
    path: "/billetera/deposito",
    meta: { breadcrumb: true },
    name: "deposito",
    component: () =>
      import(/* webpackChunkName: "routes" */
      `@/views/ui/Deposito.vue`)
  }
];


