const Menu =  [

  { header: "BILLETERA WEB"},
  { name: "usuario", title: "USUARIO" ,titlePage: "ACTUALIZAR USUARIO", badge: "new", component: "/components/usaurio/" },
  { name: "retiro", title: "RETIRO" ,titlePage: "RETIRO DE BILLETERA", badge: "new", component: "/components/retiro/" },
  { name: "deposito", title: "DEPOSITO" ,titlePage: "DEPOSITO DE BILLETERA", badge: "new", component: "/components/deposito/" },

];
// reorder menu
Menu.forEach((item) => {
  if (item.items) {
    item.items.sort((x, y) => {
      let textA = x.title.toUpperCase();
      let textB = y.title.toUpperCase();
      return (textA < textB) ? -1 : (textA > textB) ? 1 : 0;
    });
  }
});

export default Menu;
