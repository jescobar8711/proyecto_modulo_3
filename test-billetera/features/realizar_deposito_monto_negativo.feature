Feature: Realizar deposito a Billetera con monto negativo
   Como un cliente de API WEB (no humano)
   Se quiere Realizar un deposito con monto negativo

  Scenario: Realizo un nuevo deposito con monto negativo
   Given Con el sigueinte monto negativo -5 
    When Preparo el JSON de los datos de monto deposito negativo
    Then Hago un request POST hacia el url billetera deposito negativo con los datos
    Then Recibo una respuesta de url billetera deposito con error verdadero
    Then Recibo una respuesta de url billetera deposito con mensaje deposito no realizado