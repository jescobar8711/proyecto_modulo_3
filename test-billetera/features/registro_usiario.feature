Feature: Registra un nuevo usuario
   Como un cliente de API WEB (no humano)
   Requiero registrar un nuevo usuario

  Scenario: Registro de un usuario
   Given Los siguientes datos  CI "6392312" NOMBRE "JESUS ESCOBAR" 
    When Preparo el JSON de los datos
    Then Hago un request POST hacia el url usuario con los datos
    Then Recibo una respuesta con http status 200
    