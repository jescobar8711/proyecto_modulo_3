const { Given, When, Then } = require('cucumber')
const { expect } = require('chai')
const httpClient = require('request-promise')

let newDeposito = {};
let httpOptions = {};
let urbanagerResponse = undefined;


Given('Con el sigueinte monto a retirar {int}', function (montoDeposito) {
    // Write code here that turns the phrase above into concrete actions
    newDeposito={
        montoDeposito:montoDeposito
    };
  });

  When('Preparo el JSON de los datos de monto deposito', function () {
    // Write code here that turns the phrase above into concrete actions
    console.log('DONE');
    httpOptions = {
      method: 'POST',
      uri: 'http://localhost:85/api/billetera/deposito',
      json: true,
      body: newDeposito,
      resolveWithFullResponse: true
    };
  });


  Then('Hago un request POST hacia el url billetera deposito con los datos', async function () {
    // Write code here that turns the phrase above into concrete actions
    await httpClient(httpOptions)
    .then(function(response) {
      urbanagerResponse = response;
    })
    .catch(function(error) {
      urbanagerResponse = error;
    })
  });

  Then('Recibo una respuesta de url billetera deposito con http status {int}', function (httpStatus) {
    // Write code here that turns the phrase above into concrete actions
    expect(urbanagerResponse.statusCode).to.eql(httpStatus);
  });
