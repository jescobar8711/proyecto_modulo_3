const { Given, When, Then } = require('cucumber')
const { expect } = require('chai')
const httpClient = require('request-promise')

let newRetiro = {};
let httpOptions = {};
let urbanagerResponse = undefined;


When('Preparo el JSON de los datos de consulta de saldo', function () {
  // Write code here that turns the phrase above into concrete actions
  console.log('DONEa');
  httpOptions = {
    method: 'GET',
    uri: 'http://localhost:85/api/billetera',
    json: true,
    resolveWithFullResponse: true
  };
});
Then('Hago un request GET hacia el url del billetera', async function () {
  // Write code here that turns the phrase above into concrete actions
  await httpClient(httpOptions)
  .then(function(response) {
    urbanagerResponse = response;
  })
  .catch(function(error) {
    urbanagerResponse = error;
  })
});

Then('Recibo una respuesta de url billetera consulta de saldo con http status {int}', function (httpStatus) {
  // Write code here that turns the phrase above into concrete actions
  expect(urbanagerResponse.statusCode).to.eql(httpStatus);
});