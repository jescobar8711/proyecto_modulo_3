const { Given, When, Then } = require('cucumber')
const { expect } = require('chai')
const httpClient = require('request-promise')

let newRetiro = {};
let httpOptions = {};
let urbanagerResponse = undefined;


Given('Con el sigueinte monto {int}', function (montoRetiro) {
    // Write code here that turns the phrase above into concrete actions
    newRetiro={
      montoRetiro:montoRetiro
    };
  });

  When('Preparo el JSON de los datos de monto retiro', function () {
    // Write code here that turns the phrase above into concrete actions
    console.log('DONE');
    httpOptions = {
      method: 'POST',
      uri: 'http://localhost:85/api/billetera/retiro',
      json: true,
      body: newRetiro,
      resolveWithFullResponse: true
    };
  });


  Then('Hago un request POST hacia el url billetera retiro con los datos', async function () {
    // Write code here that turns the phrase above into concrete actions
    await httpClient(httpOptions)
    .then(function(response) {
      urbanagerResponse = response;
    })
    .catch(function(error) {
      urbanagerResponse = error;
    })
  });

  Then('Recibo una respuesta de url billetera retiro con http status {int}', function (httpStatus) {
    // Write code here that turns the phrase above into concrete actions
    expect(urbanagerResponse.statusCode).to.eql(httpStatus);
  });
