  
const { Given, When, Then } = require('cucumber')
const { expect } = require('chai')
const { Builder, By, Key, until } = require('selenium-webdriver');
let deposito = 0;
let chromeDriver = undefined;
Given('Dado el monto de deposito :{int}', function (int) {
  // Write code here that turns the phrase above into concrete actions
  deposito = int;
});

When('Navego a la pagina de billetera deposito', async function () {
  // Write code here that turns the phrase above into concrete actions
  chromeDriver = await new Builder().forBrowser('firefox').build();
  await chromeDriver.get('http://localhost:8080/#/billetera/deposito/');
});

When('Llenar el campo de deposito',async function () {
  // Write code here that turns the phrase above into concrete actions
  await chromeDriver.findElement(By.name('depositoInput')).sendKeys(deposito);
});

When('Hacer click en el boton guardar',async function () {
  // Write code here that turns the phrase above into concrete actions
  await chromeDriver.findElement(By.name('submit')).click();
});

Then('Se debe mostrar el saldo actualizado:{int}',async function (int) {
  // Write code here that turns the phrase above into concrete actions
  await chromeDriver.findElement(By.name('saldo'))
  .getText().then(function (text) {
    showText = text;
  });

expect(showText).to.eql(expected);
await chromeDriver.quit();
});