const { Given, When, Then } = require('cucumber')
const { expect } = require('chai')
const httpClient = require('request-promise')

let newUser = {};
let httpOptions = {};
let urbanagerResponse = undefined;


Given('Los siguientes datos  CI {string} NOMBRE {string}', function (Ci, Nombre) {
    // Write code here that turns the phrase above into concrete actions
   newUser={
       Ci:Ci,
       Nombre:Nombre
   }
  });


  When('Preparo el JSON de los datos', function () {
    // Write code here that turns the phrase above into concrete actions
    console.log('DONE');
    httpOptions = {
      method: 'POST',
      uri: 'http://localhost:85/api/usuario',
      json: true,
      body: newUser,
      resolveWithFullResponse: true
    };
  });
  Then('Hago un request POST hacia el url usuario con los datos', async function () {
    // Write code here that turns the phrase above into concrete actions
    await httpClient(httpOptions)
    .then(function(response) {
      urbanagerResponse = response;
    })
    .catch(function(error) {
      urbanagerResponse = error;
    })
  });
  Then('Recibo una respuesta con http status {int}', function (httpStatus) {
    // Write code here that turns the phrase above into concrete actions
    expect(urbanagerResponse.statusCode).to.eql(httpStatus);
  });

