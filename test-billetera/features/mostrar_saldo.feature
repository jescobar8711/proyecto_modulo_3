Feature: Mostrar informacion del saldo de billetera luego del un deposito
 Como Usuario Final (humano)
 Quiero ver los datos de un saldo del sistema

 Scenario: Se tiene datos validos y se muestran correctamente
  Given Dado el monto de deposito :50
   When Navego a la pagina de billetera deposito
     And Llenar el campo de deposito
     And Hacer click en el boton guardar
     Then Se debe mostrar el saldo actualizado:50