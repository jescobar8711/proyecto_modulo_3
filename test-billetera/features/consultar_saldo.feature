Feature: Consulta saldo de billetera
   Como un cliente de API WEB (no humano)
   Requiero consultar saldo de billetera

  Scenario: Requiero consultar Saldo
    When Preparo el JSON de los datos de consulta de saldo
    Then Hago un request GET hacia el url del billetera
    Then Recibo una respuesta de url billetera consulta de saldo con http status 200