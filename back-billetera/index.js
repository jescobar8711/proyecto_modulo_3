const express = require("express");
const path = require("path");
const bodyParser = require('body-parser');
const app = express();

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
// parse various different custom JSON types as JSON
app.use(bodyParser.json({ type: 'application/json' }))
// Add headers
app.use(function (req, res, next) {

  // Website you wish to allow to connect
  res.setHeader('Access-Control-Allow-Origin', 'http://localhost:8080');

  // Request methods you wish to allow
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');

  // Request headers you wish to allow
  res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');

  // Set to true if you need the website to include cookies in the requests sent
  // to the API (e.g. in case you use sessions)
  res.setHeader('Access-Control-Allow-Credentials', true);

  // Pass to next layer of middleware
  next();
});


let usuario = {
 Ci:'',
 Nombre:''
};

let billetera = {
  saldo:0
 };

let respuesta = {
 error: false,
 codigo: 200,
 mensaje: '',
 data:''
};
app.get('/api/', function(req, res) {
 respuesta = {
  error: false,
  codigo: 200,
  mensaje: 'Bienvenido al server billetera API REST',
  data:''
 };
 res.send(respuesta);
});

app.get('/api/usuario', function (req, res) {
  if(usuario.Ci === '' || usuario.Nombre === '') {
   respuesta = {
    error: true,
    codigo: 501,
    mensaje: 'El usuario no ha sido creado ',
    data:''
   };
  } else {
   respuesta = {
    error: false,
    codigo: 200,
    mensaje: 'Datos de Usuario Actual',
    data: usuario,
   };
  }
  res.send(respuesta);
 });

app.post('/api/usuario', function (req, res) {
 if(!req.body.Ci || !req.body.Nombre) {
  respuesta = {
   error: true,
   codigo: 502,
   mensaje: 'El campo Ci y Nombre son Obligatorio'
  };
 } else {
 
   usuario = {
    Ci: req.body.Ci,
    Nombre: req.body.Nombre
   };
   respuesta = {
    error: false,
    codigo: 201,
    mensaje: 'Usuario Actulizado Correctamente',
    data: usuario,
   };
 }
 
 res.send(respuesta);
});

app.get('/api/billetera', function (req, res) {
  if(usuario.Ci === '') {
   respuesta = {
    error: true,
    codigo: 501,
    mensaje: 'El usuario no ha sido creado',
    data: {'usuario':usuario,'billetera':billetera},
   };
  } else {
   respuesta = {
    error: false,
    codigo: 200,
    mensaje: 'Datos de Usuario Actual',
    data: {'usuario':usuario,'billetera':billetera},
   };
  }
  res.send(respuesta);
 });

 app.post('/api/billetera/deposito', function (req, res) {
  if(usuario.Ci === '') {
   respuesta = {
    error: true,
    codigo: 501,
    mensaje: 'El usuario no ha sido creado',
    data: {'usuario':usuario,'billetera':billetera},
   };
  } else {
    if(parseFloat(req.body.montoDeposito) > 0) {
      billetera.saldo=billetera.saldo+parseFloat(req.body.montoDeposito);
   respuesta = {
    error: false,
    codigo: 200,
    mensaje: 'deposito realizado correctamente',
    data: {'usuario':usuario,'billetera':billetera},
   };
  }else{
    respuesta = {
      error: true,
      codigo: 501,
      mensaje: 'deposito no realizado.',
      data: {'usuario':usuario,'billetera':billetera},
  }
  }
}
  res.send(respuesta);
 });

 app.post('/api/billetera/retiro', function (req, res) {
  if(usuario.Ci === '') {
   respuesta = {
    error: true,
    codigo: 501,
    mensaje: 'El usuario no ha sido creado',
    data: {'usuario':usuario,'billetera':billetera},
   };
  } else {
    if(parseFloat(req.body.montoRetiro) > 0 && billetera.saldo>=parseFloat(req.body.montoRetiro) ) {
      billetera.saldo=billetera.saldo-parseFloat(req.body.montoRetiro);
   respuesta = {
    error: false,
    codigo: 200,
    mensaje: 'retiro realizado correctamente',
    data: {'usuario':usuario,'billetera':billetera},
   };
  }else{
    respuesta = {
      error: true,
      codigo: 501,
      mensaje: 'retiro no realizado.',
      data: {'usuario':usuario,'billetera':billetera},
  }
  }
}
  res.send(respuesta);
 });

app.use(function(req, res, next) {
 respuesta = {
  error: true, 
  codigo: 404, 
  mensaje: 'URL no encontrada',
  data:''
 };
 res.status(404).send(respuesta);
});
app.disable('x-powered-by');
app.listen(85, () => {
 console.log("El servidor está iniCializado en el puerto 85");
});